
Drupal.behaviors.uc_dynamicproductpages = function(context) {

  $('.add-to-cart .attributes select', context).change(function(event) {    
    var optionNumber = $(this).val();
    var attribute    = $(this).parents('.attribute');
    
    attribute.find('.dynamic-product-attribute').hide();
    attribute.find('.attribute-option-' + optionNumber).fadeIn();
  });
}
